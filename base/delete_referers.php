<?php

/**
 * Supprimer les referers
 *
 * Surcharge pour prendre en compte tous les objets éditoriaux
 *
 * @plugin    Statistiques des objets éditoriaux
 * @copyright 2016
 * @author    tcharlss
 * @licence   GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
} // securiser

// faudrait plutot recuperer dans inc_serialbase et inc_auxbase
// mais il faudra prevenir ceux qui affectent les globales qui s'y trouvent
// Afficher la liste de ce qu'on va detruire et demander confirmation
// ca vaudrait mieux

/**
 * Supprimer les referers
 *
 * @param strinf $titre
 * @param bool $reprise
 * @return string
 */
function base_delete_referers_dist($titre = '', $reprise = '') {
	if (!$titre) {
		return;
	} // anti-testeur automatique
	sql_delete('spip_referers');
	sql_delete('spip_referers_articles');
	sql_delete('spip_referers_objets');

	include_spip('inc/statistiques');
	statistiques_reset_stats_objets(['referers' => 0]);

	// un pipeline pour detruire les tables de referers installees par les plugins ?
	//pipeline('delete_referers', '');

	spip_log('raz des referers operee redirige vers ' . _request('redirect'));
}