# Statistiques des objets
![statsobjets](./prive/themes/spip/images/statsobjets-xx.svg)

Un plugin qui étend les statistiques de SPIP à tous les types de contenus.

SPIP intègre un système simple permettant de compter et suivre le nombre de visites.
Sont comptées les visites globales sur toutes les pages du site, ainsi que le détail des visites sur les articles.

Les autres types de contenus ne sont pas pris en compte : rubriques, brèves, patates, etc.

Ce plugin remédie à cela en permettant d’étendre le détail des visites à tous les autres types d’objets éditoriaux.


Documentation officielle:\
https://contrib.spip.net/5189
