<?php
// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'statsobjets_nom' => 'Statistiques des objets',
	'statsobjets_slogan' => 'Étendre les statiques de SPIP à tous les objets éditoriaux',
	'statsobjets_description' => 'Ce plugin permet d\'activer les statistiques pour tous les objets éditoriaux, en plus des articles.
	Les objets à prendre en compte sont activables dans [le formulaire de configuration des statistiques->?exec=configurer_avancees#formulaire_configurer_compteur], dans le menu « Configuration &rarr; Fonctions avancées ». Si vous aviez activé les statistiques avant d\'installer ce plugin, les articles seront automatiquement activés.
	Les visites des articles continueront à être stockées dans la table spip_visites_articles, les visites des autres objets sont stockées dans la table spip_visites_objets.',

);
?>
