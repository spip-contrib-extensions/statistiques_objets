<?php

/**
 * Source pour les itérateurs : visites d'un type objet éditorial (optionnel)
 *
 * Adaptation de stats_visites_to_array.php pour prendre en compte tous les objets éditoriaux
 *
 * @plugin    Statistiques des objets éditoriaux
 * @copyright 2016
 * @author    tcharlss
 * @licence   GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function inc_stats_visites_objet_to_array_dist($unite, ?int $duree = null, ?string $objet = null, ?int $id_objet = null) {
	$stats_visites_to_array = charger_fonction('stats_visites_to_array', 'inc');
	return $stats_visites_to_array($unite, $duree, $objet, $id_objet);
}