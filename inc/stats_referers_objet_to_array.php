<?php

/**
 * Source pour les itérateurs : referers d'un type objet éditorial (optionnel).
 *
 * Adaptation de stats_referers_to_array.php pour prendre en compte tous les objets éditoriaux
 *
 * @plugin    Statistiques des objets éditoriaux
 * @copyright 2016
 * @author    tcharlss
 * @licence   GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function inc_stats_referers_objet_to_array_dist($limit, ?string $jour = null, ?string $objet = null, ?int $id_objet = null, $options = []) {
	$stats_referers_to_array = charger_fonction('stats_referers_to_array', 'inc');
	return $stats_referers_to_array($limit, $jour, $objet, $id_objet, $options);
}